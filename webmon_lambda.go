package main

import (
    "context"
    "os"

    "github.com/aws/aws-lambda-go/lambda"
    "github.com/aws/aws-sdk-go-v2/aws"
)

func HandleRequest(ctx context.Context) error {
    checkSite(aws.String(os.Getenv("WEBMON_SITE")),aws.String(os.Getenv("WEBMON_NAMESPACE")))
    return nil
}

func main() {
    lambda.Start(HandleRequest)
}
