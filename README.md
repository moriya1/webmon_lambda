# WEB サイトの死活監視用

Cloudwatch で WEB サイトの死活監視をしたい。

ec2 を立ち上げて cron で監視するとなると、その分課金される。

EventBridge から定期的に ECS タスク、または、AWS バッチを起動する方法も試してみたが、起動〜実行までに1分ほどかかるようだった。

EventBridge から定期的に Lambda 関数を呼び出せば、起動時間も早いのではないかと思い、この方法を試してみた。

node や python でも良いが、golang であれば実行バイナリを一つアップロードするだけで良くデプロイが簡単であろうと思い、golang で記述した。

## ビルド

```
docker run --rm -v ~/go:/go -v $(pwd):/usr/src/go -w /usr/src/go -e GOPATH=/go golang:latest go build
```

ビルドすると、webmon_lambda という実行ファイルができる。

## デプロイ

1. webmon_lambda を zip で圧縮する。
1. aws コンソールから、AWS Lambda にアクセスし、「関数を作成する」を押す。
1. 関数名に webmon など適当な名前を指定する。
1. ランタイムに Go 1.x を指定する。
1. アーキテクチャに x86_64 を指定する。(x86_64 でビルドした場合)
1. 環境変数を設定する。(後述)
1. 「関数を作成」ボタンを押す。
1. ロールに CloudwatchFullAccess を追加。(もっと細かい設定をしたほうが良いと思うが。)
1. 「コード」から zip ファイルをアップロードする。ランタイム設定でハンドラを webmon_lambda に設定する。
1. 「トリガーを追加」で、EventBridge を選び、[スケジュール式](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/events/ScheduledEvents.html) を設定する。

設定する環境変数
```
WEBMON_NAMESPACE Webmon
WEBMON_SITE WEBサイトURL
```

